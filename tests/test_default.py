import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('default_group')


def test_notebook_running(Command, Sudo):
    with Sudo():
        cmd = Command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['notebook']


def test_notebook_index(Command):
    cmd = Command('curl -L http://localhost:8888')
    assert '<title>Jupyter Notebook</title>' in cmd.stdout
