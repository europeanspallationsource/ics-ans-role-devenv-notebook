ics-ans-role-devenv-notebook
============================

Ansible role to install jupyter notebook in the devenv.

Requirements
------------

- ansible >= 2.3
- molecule >= 1.24

Role Variables
--------------

```yaml
devenv_notebook_image: europeanspallationsource/notebook
devenv_notebook_tag: latest
devenv_notebook_port: 7878
devenv_notebook_app_iopub_data_rate_limit: 0
devenv_notebook_app_password: "sha1:8cb4b6d00188:4d31fb302487e8660affeefd48d2a8323f2ac4a2"
devenv_notebook_app_token: ""
```


Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-devenv-notebook
```

License
-------

BSD 2-clause
